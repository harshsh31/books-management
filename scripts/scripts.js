document.addEventListener("DOMContentLoaded",function(){

    //storage variable created
    var addbooks = document.getElementById('addbooks');
    var books = document.getElementById('books');
    var favorites = document.getElementById('favorites');
    var storage = window.localStorage;
    
    //getting values from the addbooks section
    var submit = document.getElementsByClassName('submit')[0];
    var booksadded = [];

    function initBooks(){
        for(let i=0;i<storage.length-1;i++){
            if(JSON.parse(storage.getItem("book" + i)))
                booksadded.push(JSON.parse(storage.getItem("book" +i)));
        }
    }

    submit.addEventListener('click',function(event){
        if (!storage.clickcount || isNaN(storage.clickcount)){
            storage.clickcount = 0;
        }
        var count = Number(storage.clickcount) ;
        var bname = document.getElementsByName('bookname')[0];
        var aname = document.getElementsByName('author')[0];
        var bprice = document.getElementsByName('bookprice')[0];
        if(!(bname.value || aname.value || bprice.value)){
            alert('Some fields are not filled');
        }
        else{
            var book = {
                'bookname' : bname.value,
                'author' : aname.value,
                'bookprice' : bprice.value,
                'favorites' : 0
            };
            storage.setItem("book" + count,JSON.stringify(book));
            storage.clickcount = Number(storage.clickcount) + 1;
            bname.value = "";
            aname.value = "";
            bprice.value = "";
            booksadded.push(book);
            updateValues();
            alert('Book successfully added!');
        }
        
    });


    function updateValues(){
        //getting the value for the books and the favorites span tag
        var nbooks = document.getElementsByClassName('totalbooks')[0];
        var favcount = 0;
        var nfav = document.getElementsByClassName('totalfavorites')[0];
        nbooks.innerHTML = booksadded.length;
        for(let i=0; i < booksadded.length;i++){
            if(booksadded[i].favorites)
                favcount++;
        }
        nfav.innerHTML = favcount;
        updateBooksAdded();
        updateFavoriteBooks();
    }

    //function for dynamically picking up the books and creating a div for each book element
    function updateBooksAdded(){
        var e = document.getElementsByClassName('book-content')[0];
        if(e.hasChildNodes){
            var child = e.lastElementChild;  
            while (child) { 
                e.removeChild(child); 
                child = e.lastElementChild; 
            } 
        }
        for(let i = 0; i<booksadded.length ; i++){
            var d = document.getElementsByClassName('booktag')[0].cloneNode(true);
            d.style.display = 'initial';
            var h2 = document.createElement('h2');
            var h3 = document.createElement('h3');
            var price  = document.createElement('span');
            price.className = 'price';
            h2.textContent = booksadded[i].bookname;
            price.innerHTML = booksadded[i].bookprice;
            h3.textContent = booksadded[i].author;
            h3.appendChild(price);
            d.firstElementChild.append(h2);
            d.firstElementChild.append(h3);
            document.getElementsByClassName('book-content')[0].append(d);
        }
        //add favs working
        var addfavs = document.querySelectorAll('.book-content .booktagcon p .addtofav');
        for(let i = 0; i<addfavs.length; i++){
            addfavs[i].addEventListener("click",function(){
                var img = addfavs[i].previousElementSibling;
                var book = JSON.parse(storage.getItem("book"+i));
                if(booksadded[i].favorites  == 0){
                    console.log(booksadded[i].favorites);
                    booksadded[i].favorites = 1;
                    book.favorites = 1;
                    storage.setItem("book"+i,JSON.stringify(book));
                    alert("Book added to favorite");
                }
                else{
                    booksadded[i].favorites = 0;
                    book.favorites = 0;
                    storage.setItem("book"+i,JSON.stringify(book));
                    alert("Book removed from favorite");
                }

                updateValues();
                
            });
        }
        //function for deleting a book
        var del = document.querySelectorAll('.book-content .delete');
        for(let i=0;i<del.length;i++){
            del[i].addEventListener("click",function(e){
                console.log(del[i].parentElement.parentElement.lastElementChild.previousElementSibling.textContent);
                for(let j=0;j<booksadded.length;j++){
                    console.log(booksadded[j].author);
                    if(del[i].parentElement.parentElement.lastElementChild.previousElementSibling.textContent == booksadded[j].bookname){
                        console.log(j);
                        booksadded.splice(j,1);
                        storage.removeItem("book" + j);
                        storage.clickcount = Number(storage.clickcount) - 1;
                        del[i].parentElement.parentElement.parentElement.remove();
                        updateValues();
                        break;
                    }
                }
            });
        }
    }

    function updateFavoriteBooks(){
        var e = document.getElementsByClassName('favorite-content')[0];
        if(e.hasChildNodes){
            var child = e.lastElementChild;  
            while (child) { 
                e.removeChild(child); 
                child = e.lastElementChild; 
            } 
        }
        for(let i = 0; i<booksadded.length ; i++){
            if(booksadded[i].favorites == 1){
                var d = document.getElementsByClassName('favtag')[0].cloneNode(true);
                d.style.display = 'initial';
                var h2 = document.createElement('h2');
                var h3 = document.createElement('h3');
                var price  = document.createElement('span');
                price.className = 'price';
                h2.textContent = booksadded[i].bookname;
                price.innerHTML = booksadded[i].bookprice;
                h3.textContent = booksadded[i].author;
                h3.appendChild(price);
                d.firstElementChild.append(h2);
                d.firstElementChild.append(h3);
                document.getElementsByClassName('favorite-content')[0].append(d);
            }
        }
        //function for deleting a book
        var del = document.querySelectorAll('.favorite-content .delete');
        for(let i=0;i<del.length;i++){
            del[i].addEventListener("click",function(e){
                console.log(del[i].parentElement.parentElement.lastElementChild.previousElementSibling.textContent);
                for(let j=0;j<booksadded.length;j++){
                    console.log(booksadded[j].author);
                    if(del[i].parentElement.parentElement.lastElementChild.previousElementSibling.textContent == booksadded[j].bookname){
                        console.log(j);
                        booksadded[j].favorites = 0;
                        del[i].parentElement.parentElement.parentElement.remove();
                        updateValues();
                        break;
                    }
                }
            });
        }

    }
    var search = document.querySelector('.books .search-bar p input');
    search.addEventListener("keyup",function(){
        searchValue(search);
    });
    var search2 = document.querySelector('.favorites .search-bar p input');
    search2.addEventListener("keyup",function(){
        searchValue(search2);
    });
    function searchValue(search){
        if(search.value.length < 2){
            updateValues();
        }
        else{
            var e = search.parentElement.parentElement.parentElement.lastElementChild;
            if(e.hasChildNodes){
                var child = e.lastElementChild;
                while(child){
                    e.removeChild(child);
                    child = e.lastElementChild;
                }
            }
            if(search.value.length >= 2){
                for(let i = 0; i < booksadded.length; i++ ){
                    if(booksadded[i].author.toLowerCase().includes(search.value.toLowerCase())|| booksadded[i].bookname.toLowerCase().includes(search.value.toLowerCase())){
                        if(search.parentElement.parentElement.parentElement.classList.contains('books')){
                            var d = document.getElementsByClassName('booktag')[0].cloneNode(true);
                            d.style.display = 'initial';
                            var h2 = document.createElement('h2');
                            var h3 = document.createElement('h3');
                            var price  = document.createElement('span');
                            price.className = 'price';
                            h2.textContent = booksadded[i].bookname;
                            price.innerHTML = booksadded[i].bookprice;
                            h3.textContent = booksadded[i].author;
                            h3.appendChild(price);
                            d.firstElementChild.append(h2);
                            d.firstElementChild.append(h3);
                            document.getElementsByClassName('book-content')[0].append(d);
                            console.log(d);
                        }
                        else{
                            if(booksadded[i].favorites == 1){
                                var d = document.getElementsByClassName('favtag')[0].cloneNode(true);
                                d.style.display = 'initial';
                                var h2 = document.createElement('h2');
                                var h3 = document.createElement('h3');
                                var price  = document.createElement('span');
                                price.className = 'price';
                                h2.textContent = booksadded[i].bookname;
                                price.innerHTML = booksadded[i].bookprice;
                                h3.textContent = booksadded[i].author;
                                h3.appendChild(price);
                                d.firstElementChild.append(h2);
                                d.firstElementChild.append(h3);
                                document.getElementsByClassName('favorite-content')[0].append(d);
                            }
                        }
                        
                    }
                }
            }

        }
        
        

    }
    
    //writing events for the a tag click 
    var anchors = document.querySelectorAll('.items a');
    var divs = document.querySelectorAll('#main-content > div');
    var pos;
    anchors.forEach(function(anchor){
        anchor.addEventListener("click",function (e) {
            e = e.target.closest('a');
            for(let i=0; i<anchors.length;i++){
                if(e == anchors[i]){
                    pos = i;
                } 
                anchors[i].classList.remove('active');
                divs[i+1].style.display = 'none';
            }
            anchor.classList.add('active');
            divs[pos+1].style.display = 'initial';
        });
    });
    initBooks();
    updateValues();
});